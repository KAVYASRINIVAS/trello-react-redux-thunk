import React from "react";
import { Link } from "react-router-dom";
import logo from "../images/header-logo-spirit.d947df93bc055849898e.gif";

function Header() {
  return (
    <div className="d-flex align-items-center Header">
      <Link to="/boards">
        <img src={logo} alt=""></img>
      </Link>
    </div>
  );
}

export default Header;
