import React, { Component } from "react";
import { connect } from "react-redux";
import { GetBoardsAction, AddBoardAction } from "../BoardActions";
import BoardsList from "./BoardsList";
import PopUp from "./PopUp";

class BoardContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      spinner: true,
    };
  }

  componentDidMount() {
    this.props.dispatch(GetBoardsAction());
    this.setState({
      spinner: false,
    });
  }

  handleModal = () => {
    this.setState({
      show: !this.state.show,
    });
  };

  handleTitle = (newTitle) => {
    this.props.dispatch(AddBoardAction(newTitle));
  };

  render() {
    return (
      <div className="d-flex flex-column">
        {this.state.show === true ? (
          <PopUp
            toggleShow={this.handleModal}
            handleBoardTitle={this.handleTitle}
          />
        ) : (
          ""
        )}
        <BoardsList
          boardData={this.props.boards}
          toggleShow={this.handleModal}
          showBoard={this.handleShowBoard}
          spinner={this.state.spinner}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    boards: state.BoardsReducer,
  };
};

export default connect(mapStateToProps)(BoardContainer);
