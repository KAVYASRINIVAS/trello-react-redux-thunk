import React, { Component } from "react";
import CheckItem from "./CheckItem";
import { Form } from "react-bootstrap";
import { connect } from "react-redux";
import {
  GetCheckitems,
  AddCheckitem,
  DeleteCheckitem,
  UpdateCheckitem,
} from "../CheckitemAction";

class CheckList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkItemTitle: "",
      addItem: false,
      isDataLoaded: false,
    };
  }

  componentDidMount() {
    this.props.dispatch(GetCheckitems(this.props.checkListData.id)).then(() => {
      this.setState({
        isDataLoaded: true,
      });
    });
  }

  handleTitle = (e) => {
    this.setState({
      checkItemTitle: e.target.value,
    });
  };

  handleAddItem = () => {
    this.setState({
      addItem: !this.state.addItem,
    });
  };

  createCheckItem = (id, title) => {
    if (title !== "") {
      this.props.dispatch(AddCheckitem(id, title)).then(() => {
        this.setState({
          addItem: !this.state.addItem,
          checkItemTitle: "",
        });
      });
    }
  };

  handleDelete = (listId, itemId) => {
    this.props.dispatch(DeleteCheckitem(listId, itemId));
  };

  handleUpdate = (cardID, checklistId, itemId, status) => {
    this.props.dispatch(UpdateCheckitem(cardID, checklistId, itemId, status));
  };

  render() {
    return (
      <section className="p-2 m-3 rounded fw-bolder CheckLists">
        <div className="d-flex flex-column m-3 justify-content-between ChecklistContainer">
          <div className="d-flex justify-content-between CheckList">
            <span>{this.props.checkListData.name}</span>
            <button
              onClick={() =>
                this.props.deleteCheckList(this.props.checkListData.id)
              }
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                fill="currentColor"
                className="bi bi-trash"
                viewBox="0 0 16 16"
              >
                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"></path>
                <path
                  fillRule="evenodd"
                  d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"
                ></path>
              </svg>
            </button>
          </div>

          <div className="d-flex flex-column CheckItems">
            {this.state.isDataLoaded === true
              ? this.props.checkitems[this.props.checkListData.id].map(
                  (item) => (
                    <CheckItem
                      className="d-flex flex-column"
                      key={item.id}
                      cardID={this.props.cardId}
                      checklistID={this.props.checkListData.id}
                      checkItemData={item}
                      deleteCheckItem={this.handleDelete}
                      updateItem={this.handleUpdate}
                    />
                  )
                )
              : ""}

            {this.state.addItem === true ? (
              <section className="newCheckList">
                <Form
                  className="d-flex flex-column p-2 justify-content-between "
                  onSubmit={(e) => {
                    e.preventDefault();
                    this.createCheckItem(
                      this.props.checkListData.id,
                      this.state.checkItemTitle
                    );
                  }}
                >
                  <input
                    type="text"
                    autoFocus
                    placeholder="Enter checkitem title"
                    value={this.state.checkItemTitle}
                    onChange={this.handleTitle}
                  />
                  <div className="m-2">
                    <button
                      className="m-2 border-0 bg-transparent fw-bold"
                      onClick={this.handleDelete}
                    >
                      Create
                    </button>
                  </div>
                </Form>
              </section>
            ) : (
              <button
                className="m-2 rounded border-0 text-white fw-bold itemBtn"
                onClick={this.handleAddItem}
              >
                + Add Item
              </button>
            )}
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    checkitems: state.CheckitemReducer,
  };
};

export default connect(mapStateToProps)(CheckList);
