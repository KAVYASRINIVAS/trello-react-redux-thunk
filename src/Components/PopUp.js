import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import Modal from 'react-bootstrap/Modal';

class PopUp extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            title: ""
        }
    }

    newTitle = (e) => {
        this.setState({
            title: e.target.value
        })
    }

    render() {
        return (
            <Modal size='sm'
                show={this.props.toggleShow}
                onHide={this.props.toggleShow}
                style={{
                    fontFamily: "'Roboto Slab', serif",
                }}>

                <h6 className="fw-bold m-3">Board Title</h6>
                <form className="m-3" onSubmit={(e) => {
                    e.preventDefault();
                    this.props.handleBoardTitle(this.state.title);
                    this.props.toggleShow()
                    this.setState({
                        title: ""
                    })
                }}>
                    <input
                        className='w-100'
                        type="text"
                        placeholder="Enter Board Title"
                        value={this.state.title}
                        onChange={this.newTitle}
                        autoFocus
                    />
                    <button type="submit" className="mt-2 text-white border-0 p-1 w-25 rounded" style={{ backgroundColor: "#026aa7" }}>Add</button>
                    <button type="submit" className="mt-2 mx-2 text-white border-0 p-1 w-25 rounded bg-secondary" onClick={this.props.toggleShow}>Close</button>
                </form>
            </Modal>
        );
    }
}

export default PopUp;