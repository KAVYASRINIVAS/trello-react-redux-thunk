import { combineReducers } from "redux";
import * as ACTIONS from "./ActionTypes"

export const BoardsReducer = (boards = [], action) => {
    switch (action.type) {
        case ACTIONS.GET_BOARDS:
            return [...action.boards];

        case ACTIONS.ADD_BOARDS:
            return [...boards, { ...action.boards }];

        default:
            return boards;
    }
}

const ListReducer = (lists = [], action) => {
    switch (action.type) {
        case ACTIONS.GET_LISTS:
            return [...action.lists];

        case ACTIONS.ADD_LIST:
            return [...lists, { ...action.lists }];

        case ACTIONS.DELETE_LIST:
            return lists.filter(list => list.id !== action.id)
        default:
            return lists
    }

}

const CardReducer = (cards = {}, action) => {
    switch (action.type) {
        case ACTIONS.GET_CARDS:
            // console.log(cards)
            return { ...cards, [action.listId]: action.cards };

        case ACTIONS.ADD_CARD:
            return { ...cards, [action.listId]: [...cards[action.listId], action.cards] };

        case ACTIONS.DELETE_CARD:
            return { ...cards, [action.listId]: cards[action.listId].filter(card => card.id !== action.id) }
        default:
            return cards;
    }

}

const ChecklistReducer = (checklists = [], action) => {
    switch (action.type) {
        case ACTIONS.GET_CHECKLISTS:
            return [...action.checklists];

        case ACTIONS.ADD_CHECKLIST:
            return [...checklists, { ...action.checklists }];

        case ACTIONS.DELETE_CHECKLIST:
            return checklists.filter(checklist => checklist.id !== action.id)
        default:
            return checklists;
    }

}

const CheckitemReducer = (checkitems = {}, action) => {
    switch (action.type) {
        case ACTIONS.GET_CHECKITEMS:
            return { ...checkitems, [action.checklistId]: action.checkitems };

        case ACTIONS.ADD_CHECKITEM:
            return { ...checkitems, [action.checklistId]: [...checkitems[action.checklistId], action.checkitems] };

        case ACTIONS.DELETE_CHECKITEM:
            return { ...checkitems, [action.checklistId]: checkitems[action.checklistId].filter(checkitem => checkitem.id !== action.id) }

        case ACTIONS.UPDATE_CHECKITEM:
            // let updatedData = { ...checkitems }
            // updatedData[action.checklistId] = updatedData[action.checklistId].map(checkitem => checkitem.id === action.id ? action.checkitems : checkitem)
            // console.log(updatedData);
            return { ...checkitems, [action.checklistId]: checkitems[action.checklistId].map(checkitem => checkitem.id === action.id ? action.checkitems : checkitem) }

        default:
            return checkitems;
    }

}

export default combineReducers({
    BoardsReducer,
    ListReducer,
    CardReducer,
    ChecklistReducer,
    CheckitemReducer
})
