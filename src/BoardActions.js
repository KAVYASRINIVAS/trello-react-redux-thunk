import * as ACTIONS from "./ActionTypes"
import { getBoards, createBoard } from "./api"

export const GetBoardsAction = () => {
    return (dispatch) => {
        return getBoards().then((boards) => {
            dispatch({
                type: ACTIONS.GET_BOARDS,
                boards: boards
            })
        })
    }
}

export const AddBoardAction = (newTitle) => {
    return (dispatch) => {
        createBoard(newTitle).then((data) => {
            dispatch({
                type: ACTIONS.ADD_BOARDS,
                boards: data
            })
        })
    }

}
