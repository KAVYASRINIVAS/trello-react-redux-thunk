import *  as ACTIONS from './ActionTypes'
import { getCheckItem, createCheckItem, deleteCheckItem, updateCheckItem } from './api'

export const GetCheckitems = (checklistId) => {
    return (dispatch) => {
        return getCheckItem(checklistId).then(checkitems => {
            dispatch({
                type: ACTIONS.GET_CHECKITEMS,
                checkitems: checkitems,
                checklistId: checklistId
            })
        })
    }
}

export const AddCheckitem = (checklistId, title) => {
    return (dispatch) => {
        return createCheckItem(checklistId, title).then((data) => {
            dispatch({
                type: ACTIONS.ADD_CHECKITEM,
                checkitems: data,
                checklistId: checklistId
            })
        })
    }
}

export const DeleteCheckitem = (checklistId, id) => {
    return (dispatch) => {
        return deleteCheckItem(checklistId, id).then((data) => {
            dispatch({
                type: ACTIONS.DELETE_CHECKITEM,
                id: id,
                checklistId: checklistId
            })
        })
    }
}

export const UpdateCheckitem = (cardId, checklistId, itemId, status) => {
    return (dispatch) => {
        return updateCheckItem(cardId, itemId, status).then(data => {
            dispatch({
                type: ACTIONS.UPDATE_CHECKITEM,
                id: itemId,
                checklistId: checklistId,
                checkitems: data
            })
        })
    }
}