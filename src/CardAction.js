import *  as ACTIONS from './ActionTypes';
import { getCards, createCard, deleteCard } from './api';

export const GetCards = (listId) => {
    return (dispatch) => {
        return getCards(listId).then((cards) => {
            dispatch({
                type: ACTIONS.GET_CARDS,
                cards: cards,
                listId: listId,
            })
        })
    }
}

export const AddCard = (listId, title) => {
    return (dispatch) => {
        return createCard(listId, title).then((data => {
            dispatch({
                type: ACTIONS.ADD_CARD,
                cards: data,
                listId: listId,
            })
        }))
    }
}

export const DeleteCard = (listId, id) => {
    return (dispatch) => {
        return deleteCard(id).then((data) => {
            dispatch({
                type: ACTIONS.DELETE_CARD,
                id: id,
                listId: listId
            })
        })
    }
}