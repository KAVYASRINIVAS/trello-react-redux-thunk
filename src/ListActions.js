import *  as ACTIONS from './ActionTypes';
import { getLists, createLists, deleteList } from './api';

export const GetLists = (id) => {
    return (dispatch) => {
        return getLists(id).then((lists) => {
            dispatch({
                type: ACTIONS.GET_LISTS,
                lists: lists
            })
        })
    }

}

export const AddList = (id, title) => {
    return (dispatch) => {
        return createLists(id, title).then((data) => {
            dispatch({
                type: ACTIONS.ADD_LIST,
                lists: data
            })
        })
    }
}

export const DeleteList = (id) => {
    return (dispatch) => {
        return deleteList(id).then(res => {
            dispatch({
                type: ACTIONS.DELETE_LIST,
                id: id
            })
        })
    }
}