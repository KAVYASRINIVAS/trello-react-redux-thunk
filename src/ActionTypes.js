// Board Action Types
export const GET_BOARDS = "GET_BOARDS";
export const ADD_BOARDS = "ADD_BOARDS";

// List Action Types
export const GET_LISTS = "GET_LISTS";
export const ADD_LIST = "ADD_LIST";
export const DELETE_LIST = "DELETE_LIST";

// Card Action Types
export const GET_CARDS = "GET_CARDS";
export const ADD_CARD = "ADD_CARD";
export const DELETE_CARD = "DELETE_CARD";

// CheckList Action Types
export const GET_CHECKLISTS = "GET_CHECKLISTS";
export const ADD_CHECKLIST = "ADD_CHECKLIST";
export const DELETE_CHECKLIST = "DELETE_CHECKLIST";

// CheckItems Action Types
export const GET_CHECKITEMS = "GET_CHECKITEMS";
export const ADD_CHECKITEM = "ADD_CHECKITEM";
export const DELETE_CHECKITEM = "DELETE_CHECKITEM";
export const UPDATE_CHECKITEM = "UPDATE_CHECKITEM";