import *  as ACTIONS from './ActionTypes'
import { getCheckList, createCheckList, deleteCheckList } from './api'

export const GetChecklists = (id) => {
    return (dispatch) => {
        return getCheckList(id).then((checklists) => {
            dispatch({
                type: ACTIONS.GET_CHECKLISTS,
                checklists: checklists
            })
        })
    }
}

export const AddChecklist = (cardId, title) => {
    return (dispatch) => {
        return createCheckList(cardId, title).then((data) => {
            dispatch({
                type: ACTIONS.ADD_CHECKLIST,
                checklists: data,
            })
        })
    }
}

export const DeleteChecklist = (id) => {
    return (dispatch) => {
        return deleteCheckList(id).then(res => {
            dispatch({
                type: ACTIONS.DELETE_CHECKLIST,
                id: id,
            })
        })
    }
}